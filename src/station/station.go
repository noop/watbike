package station

type Station struct {
	Altitude              string
	AvailableBikes        int
	AvailableDocks        int
	City                  string
	Id                    int
	LandMark              string
	LastCommunicationTime *string
	Latitude              float64
	Location              string
	Longitude             float64
	PostalCode            string
	StAddress1            string
	StAddress2            string
	StationName           string
	StatusKey             int
	StatusValue           string
	TestStation           bool
	TotalDocks            int
}
