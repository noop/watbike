package main

import (
	"log"
	"net/http"
	"bikeinfo"
	"encoding/json"
	"time"
)

func getInfo(pewpew chan<- bikeinfo.Bikeinfo) {
	for {
		resp, err := http.Get("http://citibikenyc.com/stations/json")
		if err != nil {
			log.Fatal("Angry fetch [ ", err, " ]")
		}
		/* Create a bike object to pass to the channel */
		var bikeinfo bikeinfo.Bikeinfo

		/* Decode the bike response */
		dec := json.NewDecoder(resp.Body)
		err = dec.Decode(&bikeinfo)
		if err != nil {
			log.Fatal("Angry log msg [ ",err," ]")
		}
		/* Pass the bikeinfo to the channel */
		pewpew <- bikeinfo

		/* Sleep for a minute */
		time.Sleep(1 * time.Minute)
	}
}

func main() {
	log.Println("Welcome to the Machine!")

	pewpew := make(chan bikeinfo.Bikeinfo, 10)

	go getInfo(pewpew)

	for bikeinfo := range pewpew {
		for _, station := range bikeinfo.StationList {
			if station.StatusKey == 1 {
				log.Println("Station loc [ ",station.StAddress1," ] Bikes [ ",station.AvailableBikes," ] Docks [ ",station.TotalDocks," ] ")
			}
		}
	}
}
