package bikeinfo

import (
	"station"
)

type Bikeinfo struct {
	ExecutionTime string
	StationList   []station.Station	`json:"stationBeanList"`
}
