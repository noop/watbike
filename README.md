A set of simple Go structs for the Citibike JSON response.

# Why?  

Why not? Why have everyone build them? 

# Requirements  
Go

# Build 
# Switch to the project root  
cd $PROJECT_ROOT  
# Make a bin directory  
mkdir bin  
  
# Set your GOPATH to include the location of the project  
go build -o bin/watbike src/watbike/watbike.go  

# Invocation  
./bin/watbike
